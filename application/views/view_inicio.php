<div class="row">
  <div class="col-sm-3">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>style/img/perfil.jpg" alt="Editar seu perfil">
      <div class="caption" align="center">
        <h3>Atualizar seu perfil</h3>
        <p>Altere sua senha, nome, email e demais dados de seu perfil</p>
        <p>
        	<a href="<?php echo base_url(); ?>main/redirecionar/1" class="btn btn-success" role="button">Atualizar seu perfil</a> 
        </p>
      </div>
    </div>
  </div>

  <div class="col-sm-3">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>style/img/usuarios.jpg" alt="Editar seu perfil">
      <div class="caption" align="center">
        <h3>Usuários</h3>
        <p>Crie um novo usuário ou veja os existentes</p>
        <p>
        	<a href="<?php echo base_url(); ?>main/redirecionar/7" class="btn btn-success" role="button">Novo Usuário</a> 
        	<a href="<?php echo base_url(); ?>main/redirecionar/2" class="btn btn-primary" role="button">Ver Usuários</a> 
        </p>
      </div>
    </div>
  </div>

  <div class="col-sm-3">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>style/img/grupos.jpg" alt="Editar seu perfil">
      <div class="caption" align="center">
        <h3>Grupos</h3>
        <p>Crie um novo grupo ou veja os existentes</p>
        <p>
        	<a href="<?php echo base_url(); ?>main/redirecionar/6" class="btn btn-success" role="button">Novo Grupo</a> 
        	<a href="<?php echo base_url(); ?>main/redirecionar/3" class="btn btn-primary" role="button">Ver Grupos</a> 
        </p>
      </div>
    </div>
  </div>
</div>