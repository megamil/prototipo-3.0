<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_webservice extends MY_Model {

		public function validar_login($valores){

			$this->db->select('id_usuario, nome_usuario, email_usuario, fk_grupo_usuario, ativo_usuario');
			$this->db->from('seg_usuarios');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('login_usuario',$valores['login_usuario']);
			$this->db->where('senha_usuario',$valores['senha_usuario']);
			$login = $this->db->get();

			if (isset($login) && !is_null($login) && $login->num_rows() == 1) {
				return $login->row();
			} else {
				return false;
			}

		}
		######################################################	
		//Editar Usuário
		######################################################	
		public function editarUsuario($valores){

			$tabela = "seg_usuarios";
			$id = 'id_usuario';
			
			$this->gerarHistorico($id,$tabela,$valores,$valores[$id]);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);

			return $this->verificarErros($this->db->error(),'Model_webservice / editarUsuario');	


		}
		######################################################	
		//Criar Usuário
		######################################################	
		public function criarUsuario($valores){

			$this->db->insert('seg_usuarios',$valores);
			return $this->verificarErros($this->db->error(),'Model_webservice / criarUsuario');	


		}
		######################################################	
		//Listar Usuários
		######################################################	
		public function listarUsuarios(){

			return $this->db->select('nome_usuario,email_usuario')
					 		->get('seg_usuarios')
					 		->result();


		}
		

	}

?>